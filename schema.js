var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
  username: String,
  password: String,
  name: String,
  age: Number,
  salary: Number
}, {
  // Make Mongoose use Unix time (seconds since Jan 1, 1970)
  timestamps: true
});

module.exports = mongoose.model('peoples', userSchema)