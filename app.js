var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mongoose = require('./db')

var getAllRouter = require('./routes/getAll')
var getByIdRouter = require('./routes/getById')
var loginRouter = require('./routes/Login')
var registerRouter = require('./routes/Register')
var updateRouter = require('./routes/Update')
var DeleteRouter = require('./routes/Delete')

var getAllProductRouter = require('./routes/products/getAll')
var createProductRouter = require('./routes/products/Create')
var updateProductRouter = require('./routes/products/Update')
var deleteProductRouter = require('./routes/products/Delete')
var cors = require('cors')


var app = express();
app.use(cors())
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/api/v1/login', loginRouter);
app.use('/api/v1/register', registerRouter);
app.use('/api/v1/users', getAllRouter);
app.use('/api/v1/users', getByIdRouter);
app.use('/api/v1/users', updateRouter);
app.use('/api/v1/users', DeleteRouter);

app.use('/api/v1/products', getAllProductRouter);
app.use('/api/v1/products', createProductRouter);
app.use('/api/v1/products', updateProductRouter);
app.use('/api/v1/products', deleteProductRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
