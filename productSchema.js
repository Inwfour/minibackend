var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var productSchema = new Schema({
  user_id: mongoose.Schema.Types.ObjectId,
  title: String,
  detail: String,
  stock: Number,
  price: Number
}, {
  timestamps: true
});

module.exports = mongoose.model('products', productSchema)