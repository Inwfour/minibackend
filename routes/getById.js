var express = require('express');
var router = express.Router();
var User = require('../schema')


/* GET users listing. */
router.get('/:id', async function(req, res, next) {
  try {
    var user = await User.findById(req.params.id)
  res.send({
    status: 'success',
    data: user
  });
  } catch (error) {
    res.send({
      status: 'error',
      message: 'Not Found'
    });
  }
});

module.exports = router;
