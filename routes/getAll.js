var express = require('express');
var router = express.Router();
var User = require('../schema')


/* GET users listing. */
router.get('/', async function(req, res, next) {
var user = await User.find()
  res.send({
    status: 'success',
    data: user
  });
});

module.exports = router;
