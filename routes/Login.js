var express = require('express');
var router = express.Router();
var User = require('../schema')

/* GET users listing. */
router.post('/', async function(req, res, next) {
  var username = await User.findOne({ username: req.body.username })
  if (username) {
    if (req.body.password === username.password) {
      res.send({
        status: 'success',
        data: username
      });
    } else {
      res.send({
        status: 'error',
        message: 'กรุณากรอกข้อมูลให้ถูกต้อง'
      });
    }
  } else {
    res.send({
      status: 'error',
      message: 'ไม่พบผู้ใช้นี้'
    });
  }
});



module.exports = router;
