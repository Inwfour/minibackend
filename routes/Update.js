var express = require('express');
var router = express.Router();
var User = require('../schema')

/* GET users listing. */
router.put('/:id', async function(req, res, next) {
  console.log(req.params.id)
  var user = {
    name: req.body.name,
    age: req.body.age,
    salary: req.body.salary
  }
  var update = await User.findByIdAndUpdate(req.params.id, {$set: user} ,{new : true })
  if (update) {
    res.send({
      status: 'success',
      data: update
    })
  } else {
    res.send({
      status: 'error',
      data: 'update failed'
    })
  }
});

module.exports = router;
