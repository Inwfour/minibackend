var express = require('express');
var router = express.Router();
var User = require('../schema')

/* GET users listing. */
router.post('/', async function(req, res, next) {
  try {
    var userlogin = await User({
      username: req.body.username,
      password: req.body.password,
      name: req.body.name,
      age: req.body.age,
      salary: req.body.salary
    })
    var checksave = await User.findOne({ username: req.body.username })
    if (!checksave) {
      var save = await userlogin.save()
      res.send({
        status: "sucecss",
        data: save
      })
    } else {
      res.send({
      status: 'error',
      message: 'ชื่อผู้ใช้นี้มีอยู่แล้ว'
    })
    }

  } catch (error) {
    throw error
  }
});

module.exports = router;
