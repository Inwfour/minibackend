var express = require('express');
var router = express.Router();
var mongoose = require('mongoose')
var User = require('../schema')

/* GET users listing. */
router.delete('/:id', async function(req, res, next) {
  try {
      var userdelete = await User.findByIdAndDelete(req.params.id)
  if (userdelete) {
    res.send({
      status: 'success',
      message: 'delete success'
    })
  }
  } catch (error) {
    res.send({
      status: 400,
      message: "delete not found"
    })
  }

});


module.exports = router;
