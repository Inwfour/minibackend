var express = require('express');
var router = express.Router();
var Product = require('../../productSchema')


/* GET users listing. */
router.get('/', async function(req, res, next) {
var product = await Product.find()
  res.send({
    status: 'success',
    data: product
  });
});

module.exports = router;
