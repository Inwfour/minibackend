var express = require('express');
var router = express.Router();
var Product = require('../../productSchema')

/* GET users listing. */
router.post('/', async function(req, res, next) {
  try {
    var product = new Product({
      user_id: req.body.user_id,
      title: req.body.title,
      detail: req.body.detail,
      stock: req.body.stock,
      price: req.body.price
    })
  
    var productCheck = await Product.findOne({ title: req.body.title })
    if (productCheck) {
      res.send({
        status: 'error',
        message: 'ข้อมูลซ้ำ'
      })
    } else {
    var save = await product.save()
    res.send({
      status: 'success',
      data: save
    })
    }
  } catch (error) {
    res.send({
      status: 'error',
      message: 'ไม่สามารถบันทึกได้'
    })
  }
});

module.exports = router;
