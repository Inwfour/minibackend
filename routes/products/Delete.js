var express = require('express');
var router = express.Router();
var Product = require('../../productSchema')

/* GET users listing. */
router.delete('/:id', async function(req, res, next) {
  var productdelete = await Product.findByIdAndDelete(req.params.id)
  if (productdelete) {
    res.send({
      status: 'success',
      message: 'delete success'
    })
  }
});

module.exports = router;
