var express = require('express');
var router = express.Router();
var Product = require('../../productSchema')

/* GET users listing. */
router.put('/:id', async function(req, res, next) {
  console.log(req.params.id)
  var product = {
    title: req.body.title,
    detail: req.body.detail,
    stock: req.body.stock,
    price: req.body.price
  }
  var update = await Product.findByIdAndUpdate(req.params.id, {$set: product} ,{new : true })
  if (update) {
    res.send({
      status: 'success',
      data: 'update success'
    })
  } else {
    res.send({
      status: 'error',
      data: 'update failed'
    })
  }
});

module.exports = router;
